# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from . import forms
# Create your views here.
def thankyou(request):
    return render(request, 'feedBackForm/ty.html')
def feedbackview(request):

    form = forms.FeedBack()


    if request.method == 'POST':
        form = forms.FeedBack(request.POST)
        if form.is_valid():
            print("Form Validataio Success and Printing Information")
            print('Student Name:', form.cleaned_data['name'])
            print('Student RollNo:', form.cleaned_data['RollNo'])
            print('Student Email:', form.cleaned_data['Email'])
            print('Student Feedback:', form.cleaned_data['Feedback'])
    return render(request, 'feedBackForm/fb.html', {'form':form})
    

