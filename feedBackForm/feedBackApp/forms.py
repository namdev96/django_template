from django import forms
from django.core import validators

class FeedBack(forms.Form):

    name = forms.CharField()
    RollNo = forms.IntegerField()
    Email = forms.EmailField()
    password = forms.CharField(widget = forms.PasswordInput)
    rpassword = forms.CharField(label = 'password(again)', widget = forms.PasswordInput)
    bot_handler = forms.CharField(required = False, widget = forms.HiddenInput)

    Feedback = forms.CharField(widget = forms.Textarea, validators = [validators.MaxLengthValidator(40), validators.MinLengthValidator(10)])

    def clean(self):
        cleaned_data = super().clean()
        bot_handler_value = cleaned_data['bot_handler']
        if len(bot_handler_value)>0:
            raise forms.ValidationError('Thanks bot')
        pwd = cleaned_data['password']
        rpwd = cleaned_data['rpassword']

        if  pwd != rpwd:
            raise forms.ValidationError('password not matched')
        