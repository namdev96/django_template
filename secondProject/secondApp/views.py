from django.shortcuts import render  
#importing loading from django template  
from django.template import loader  
# Create your views here.  
from django.http import HttpResponse  
def index(request):  
   template = loader.get_template('html files/index.html') # getting our template  
   name = {
      'student':'Priyanka Namdev'
   }
   return HttpResponse(template.render(name))       # rendering the template in HttpResponse  


