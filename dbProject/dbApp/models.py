from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Employee(models.Model):
    eno = models.IntegerField()
    ename = models.CharField(max_length = 30, blank=True, null=True)
    esal = models.FloatField()
    eaddr = models.CharField(max_length = 30,  blank=True, null=True)
    
    def __str__(self):
        return self.ename