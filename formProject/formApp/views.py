# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from . import forms

def StudentRegister(request):
    form = forms.StudentRegister() #here name should bs as same as the class name in forms.py file
    return render(request, 'formApp/registration.html', {'form':form})