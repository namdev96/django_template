# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Hydjobs(models.Model):
    date = models.DateField()
    company = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    eligibility = models.CharField(max_length = 100)
    address = models.CharField(max_length = 100)
    phoneno = models.IntegerField()

class Chennaijobs(models.Model):
    date = models.DateField()
    company = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    eligibility = models.CharField(max_length = 100)
    address = models.CharField(max_length = 100)
    phoneno = models.IntegerField()

class Punejobs(models.Model):
    date = models.DateField()
    company = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    eligibility = models.CharField(max_length = 100)
    address = models.CharField(max_length = 100)
    phoneno = models.IntegerField()

class Banglorejobs(models.Model):
    date = models.DateField()
    company = models.CharField(max_length = 100)
    title = models.CharField(max_length = 100)
    eligibility = models.CharField(max_length = 100)
    address = models.CharField(max_length = 100)
    phoneno = models.IntegerField()