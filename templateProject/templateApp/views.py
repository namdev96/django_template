# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template import loader  
from django.shortcuts import render
from django.http import HttpResponse
import datetime
# Create your views here.
def templateInfo(request):
       
      # return render(request, 'templateApp/wish.html') # getting our template  
    date = datetime.datetime.now()
    dateinfo = {'date_msg':date,'day':'monday'}
    template = loader.get_template('templateApp/wish.html') # getting our template  
    return HttpResponse(template.render(dateinfo))       # rendering the template in HttpResponse  
    