from django.shortcuts import render
from modelApp import forms
# Create your views here.

def student_view(request):
    form = forms.StudentForm()
    if request.method == 'POST':
        form = forms.StudentForm(request.POST)
        if form.is_valid():
            form.save(commit = True)
            print('form data inserted successfully')
    return render(request, 'modelApp/register.html', {'form':form})
