# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from movieApp.models import Movie

# Register your models here.
class MovieAdmin(admin.ModelAdmin):
    list_display = ['rdate', 'moviename', 'hero', 'heroine', 'rating']

admin.site.register(Movie, MovieAdmin)