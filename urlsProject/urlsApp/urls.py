from django.conf.urls import url

from urlsApp import views

urlpatterns = [
    url(r'^hydjobs', views.hydjobinfo),
    url(r'^mumjobs', views.mumjobinfo),
    url(r'^biharjobs', views.biharjobinfo),
    url(r'^punejobs', views.punejobinfo),
    url(r'londonjobs', views.londonjobinfo)



]
