# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse  
from uploadFileApp.functions import handle_uploaded_file  
from .forms import StudentForm  
def index(request):  
    
    return render(request,'index.html')  