# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.template import loader 
from django.http import HttpResponse 

# Create your views here.
def home(request):
    return render(request, 'staticApp/home.html')    
def profile(request):
    template = loader.get_template('staticApp/profile.html') # getting our template  
    return HttpResponse(template.render())     
